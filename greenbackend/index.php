<?php
declare(strict_types=1);
require __DIR__ . "/vendor/autoload.php"; // load packages installed from packages

// define root path of application
define('APPROOT', __DIR__ . '/src/');

//instantiate SPL (Standard PHP Library) Autoloader
spl_autoload_register(function ($class) {
    
    require APPROOT . "Controllers/$class.php";
   
});

require __DIR__ . "/mapping.php"; 
?>





