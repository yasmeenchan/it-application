<?php

class addDataTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

      
    /**
     * testAddData
     * This function will create mock of the OrderController
     * Here declaring the method addApiData and it will return 
     * bool(true)
     * 
     * @return void
     */
    public function testAddData()
    {
        $OrderControllerObject = $this->createMock(OrderController::class);
        $OrderControllerObject->method("addApiData")->willReturn(true);

        $OrderControllerObject
        ->expects($this->once())
        ->method("addApiData")
        ->with(["id" => 5, "name" => 'Chandan', "state" => "Karnataka", 
        "zip" => 560009, "amount" => 25.05, "qty" => 8, "item" => '8AC123']);

        $result = $OrderControllerObject->addApiData(["id" => 5, "name" => 'Chandan', "state" => "Karnataka", 
        "zip" => 560009, "amount" => 25.05, "qty" => 8, "item" => '8AC123']);

        $this->assertTrue($result);
    }
    
    /**
     * testAddDataFail
     * If we send empty array to addApiData
     * it will not add the record to csv file
     * @return void
     */
    public function testAddDataFail()
    {
        $OrderControllerObject = $this->createMock(OrderController::class);
        $OrderControllerObject->method("addApiData")->willReturn(true);

        $OrderControllerObject
        ->expects($this->once())
        ->method("addApiData")
        ->with([]);

        $result = $OrderControllerObject->addApiData([]);

        $this->assertTrue($result);

    }
}