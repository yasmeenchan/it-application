<?php

/**
 * deleteDataTest
 */
class deleteDataTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
        
    /**
     * _before
     *
     * @return void
     */
    protected function _before()
    {
    }
    
    /**
     * _after
     *
     * @return void
     */
    protected function _after()
    {
    }
    
    /**
     * testDeleteTest
     *
     * @return void
     */
    public function testDeleteTest()
    {

        $OrderControllerObject = $this->createMock(OrderController::class);
        $OrderControllerObject->method("deleteApiData")->willReturn(true);

        $OrderControllerObject
        ->expects($this->once())
        ->method("deleteApiData")
        ->with(5);

        $result = $OrderControllerObject->deleteApiData(5);

        $this->assertTrue($result);
    }
   
    /**
     * testDeleteFail
     * If 0 is passed to deleteApiData funtion it will 
     * Delete order data from csv file
     * @return void
     */
    public function testDeleteFail()
    {
        $OrderControllerObject = $this->createMock(OrderController::class);
        $OrderControllerObject->method("deleteApiData")->willReturn(false);

        $OrderControllerObject
        ->expects($this->once())
        ->method("deleteApiData")
        ->with(0);

        $result = $OrderControllerObject->deleteApiData(0);

        $this->assertFalse($result);
    }
}