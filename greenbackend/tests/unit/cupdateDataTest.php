<?php

class updateDataTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }
  
    /**
     * testUpdateData
     * updateApiData takes array as a parameter
     * with fields mentioned as following
     * @return void
     */
    public function testUpdateData()
    {
        
        $OrderControllerObject = $this->createMock(OrderController::class);

        $OrderControllerObject
        ->expects($this->once())
        ->method("updateApiData")
        ->with(["id" => 5, "name" => 'Chandan', "state" => "Karnataka", 
        "zip" => 560009, "amount" => 25.05, "qty" => 8, "item" => '8AC123'])
        ->will($this->returnValue(true));


        $result = $OrderControllerObject->updateApiData(["id" => 5, "name" => 'Chandan', "state" => "Karnataka", 
        "zip" => 560009, "amount" => 25.05, "qty" => 8, "item" => '8AC123']);

        $this->assertTrue($result);
    }
    
    /**
     * testUpdateDataFail
     * If Id is not passed in array to updateApiData
     * it will not update into the csv file
     * @return void
     */
    public function testUpdateDataFail()
    {
        $OrderControllerObject = $this->createMock(OrderController::class);

        $OrderControllerObject
        ->expects($this->once())
        ->method("updateApiData")
        ->with(["name" => 'Chandan', "state" => "Karnataka", 
        "zip" => 560009, "amount" => 25.05, "qty" => 8, "item" => '8AC123'])
        ->will($this->returnValue(false));


        $result = $OrderControllerObject->updateApiData(["name" => 'Chandan', "state" => "Karnataka","zip" => 560009, "amount" => 25.05, "qty" => 8, "item" => '8AC123']);

        $this->assertFalse($result);
    }
}